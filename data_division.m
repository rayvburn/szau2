clear all
close all

dane = load('dane.txt');

samples = length(dane);


learning_vector = zeros(1, 2);
verification_vector = zeros(1, 2);

k_learn = 0;
k_verify = 0;

for i = 1:2100

    learning_vector(i,1) = dane(i,1);
    learning_vector(i,2) = dane(i,2);

end

for i = 1:899
    verification_vector(i,1) = dane(2100+i,1);
    verification_vector(i,2) = dane(2100+i,2);

end        
        
figure
stairs(learning_vector(:,2))
title('learning data');

file_id = fopen('dane_learning.txt', 'w');
for i = 1:length(learning_vector)
    fprintf(file_id, '%5.10f %5.10f\n', learning_vector(i,1), learning_vector(i,2));
end
fclose(file_id);

figure
stairs(verification_vector(:,2));
title('verification data');

file_id = fopen('dane_verification.txt', 'w');
for i = 1:length(verification_vector)
    fprintf(file_id, '%5.10f %5.10f\n', verification_vector(i,1), verification_vector(i,2));
end
fclose(file_id);
