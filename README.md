### I

1. `zad_I_1.m`
2. `zad_I_2.m`
    - generuje plik `.txt` (csv) z sekwencją wejść i wyjść układu

---

### II

1. `Zad_II_1.txt` - `tau` układu
2. `Zad_II_test.m`: skrypt służący do weryfikacji wyuczonych modeli:
    - `Zad_II_model_*.m`: poszczególne modele z konkretną liczbą neuronów ukrytych

---