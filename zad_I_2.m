close all;
clear all;

U_MIN = -1;
U_MAX = +1;
SAMPLES = 3000-1;

proc = SimulatedProcess(-1.272717, 0.332871, 0.028400, 0.019723, 0, 0, 0, 0);

timestamps = [0.05, 0.10, 0.15, 0.20, 0.22, 0.40, 0.50, 0.60, 0.70, 0.73, 0.78, 0.80, 0.82, 0.85, 0.87, 0.90, 0.95, 0.96];
%
% V1: random control signals
% u = createControlSequence(SAMPLES, timestamps, randi(100.*[U_MIN U_MAX], 1, length(timestamps) + 1)/100.0);
%
% V2: hard-coded control signals
u = createControlSequence(SAMPLES, ...
    timestamps, ...
    [0.26, -0.1, 0.05, 0.35, 0.40, -0.5, 0.8, -0.9, 0.1, 0, -0.40, 0.0, 0.7, 0.3, 0.5, -0.6, -0.4, 0.05, -0.25] ...
);

for i = 1:length(u)
    [proc, ~, ~, ~] = proc.compute(u(i));
end

plot(1:length(proc.get_y()), proc.get_y())
hold on
plot(1:length(proc.get_u()), proc.get_u(), 'k--')
title('Przebieg procesu')
ylabel('y')
xlabel('sample')
grid on

mkdir('zad_I_2');
saveas(gcf, [pwd '/zad_I_2/przebieg.png']);

% save as csv
y = proc.get_y();
file_id = fopen('dane.txt', 'w');
for i = 1:length(u)
    fprintf(file_id, '%5.20f, %5.20f\n', u(i), y(i));
end
fclose(file_id);