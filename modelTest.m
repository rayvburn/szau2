function [y, ymod_arx, ymod_oe] = modelTest(modelParamsFileName, dataFilePath,DataType)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%   modelParamsFileName: name of the script storing model parameters
%   dataFilePath: path to the file that will be loaded
%   dataFile: file name that will be loaded (this is a part of
%             `dataFilePath`)
data = load(dataFilePath);
u = data(:, 1);
y = data(:, 2);

run(modelParamsFileName);

tau = 5;
kp =  tau;
kk = length(u);

% initialization using measurements
ymod_arx(1:kp-1) = y(1:kp-1);
ymod_oe(1:kp-1) = y(1:kp-1);

% we were asked to test ARX and OE models each time for specific `weights`
for k = kp:kk
    wesn_arx = [u(k-3) u(k-4) y(k-1) y(k-2)]';
    ymod_arx(k) = w20 + w2 * tanh(w10 + w1 * wesn_arx);

    wesn_oe = [u(k-3) u(k-4) ymod_oe(k-1) ymod_oe(k-2)]';
    ymod_oe(k) = w20 + w2 * tanh(w10 + w1 * wesn_oe);
end

Earx = (y - ymod_arx')' * (y - ymod_arx');
Eoe = (y - ymod_oe')' * (y - ymod_oe');
fprintf('model: %s, dataset: %s  --  E_arx: %4.5f, E_oe: %4.5f\n', modelParamsFileName, dataFilePath, Earx, Eoe);

% proper visibility as title
modelParamsFileName = replace(modelParamsFileName, '_', ' ');

figure;
%title_str = strcat(modelParamsFileName, ':  E_{arx} = ', num2str(Earx));
title_str = 'Model ARX';
stairs(y);
title(title_str);
hold on;
stairs(ymod_arx);

name = append('Earx_',DataType,'.png');
saveas(gcf,name)

figure;
%title_str = strcat(modelParamsFileName, ':  E_{oe} = ', num2str(Eoe));
title_str = 'Model OE';
stairs(y);
title(title_str);
hold on;
stairs(ymod_oe);

name = append('Eoe_',DataType,'.png');
saveas(gcf,name)

end