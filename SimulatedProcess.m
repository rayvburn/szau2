classdef SimulatedProcess
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = private)
        alpha1
        alpha2
        beta1
        beta2
        x1
        x2
        y
        u
    end
    
    methods
        function obj = SimulatedProcess(alpha1, alpha2, beta1, beta2, x10, x20, y0, u0)
            %UNTITLED2 Construct an instance of this class
            %   Detailed explanation goes here
            obj.alpha1 = alpha1;
            obj.alpha2 = alpha2;
            obj.beta1 = beta1;
            obj.beta2 = beta2;
            % `work point`
            obj.x1 = x10;
            % `work point`
            obj.x2 = x20;
            % `work point`
            obj.y = y0;
            % vector of controls - starting in the `work point`
            obj.u = [u0, u0, u0, u0, u0];
        end
        
        function [obj, x1_new, x2_new, y_new] = compute(obj, u)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            u_process = obj.u(end - 4);
            x1_new = -obj.alpha1 * obj.x1(end) + obj.x2(end) + obj.beta1 * obj.calculate_g1(u_process);
            x2_new = -obj.alpha2 * obj.x1(end) + obj.beta2 * obj.calculate_g1(u_process);
            y_new = obj.calculate_g2(x1_new);
            % update internal memory
            obj.x1(length(obj.x1) + 1) = x1_new;
            obj.x2(length(obj.x2) + 1) = x2_new;
            obj.y(length(obj.y) + 1) = y_new;
            obj.u(length(obj.u) + 1) = u;
        end
        
        function [g1] = calculate_g1(obj, u)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            g1 = (exp(5.25 * u) - 1) / (exp(5.25 * u) + 1);
        end
        
        function [g2] = calculate_g2(obj, x1)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            g2 = 0.8 * (1 - exp(-2 * x1));
        end
        
        function [x1] = get_x1(obj)
            x1 = obj.x1;
        end
        
        function [x2] = get_x2(obj)
           x2 = obj.x2; 
        end
        
        function [y] = get_y(obj)
            y = obj.y;
        end

        function [u] = get_u(obj)
            u = obj.u(5:end);
        end
    end
end

