% model liniowy o dynamice drugiego rzędu
% zasymulować model w trybie rekurencyjnym (predyktor OE)
clear all;
close all;

load('dane_learning.txt');
u = dane_learning(:,1);
y = dane_learning(:,2);
% liczba probek
P = length(u);

% v2: pinv
M = [u(2:P-3) u(1:P-4) y(4:P-1) y(3:P-2)];
Y = y(5:P);
w = M \ Y;
fprintf('wyliczone wagi dla modelu liniowego: (1) %4.5f, (2) %4.5f, (3) %4.5f, (4) %4.5f\n', w(1), w(2), w(3), w(4));

y_mod(1:4) = y(1:4);
for k = 5 : P
    y_mod(k) = w(1) * u(k-3) + w(2) * u(k-4) + w(3) * y_mod(k-1) + w(4) * y_mod(k-2);
end


% figure;
% stairs(y, 'b');
% hold on;
% stairs(y_mod, '--r');
% 
% y_mod = y_mod';
% E = (y_mod - y)' * (y_mod - y);
% title(strcat('E_{oe} = ', num2str(E)));
% 
% 
% figure;
% plot(y, ymod, '.');
% xlabel('Dane');
% ylabel('Model OE');
% title(strcat('Dane uczące:  E_{oe} = ', num2str(E)));

% dane weryfikujące
load('dane_verification.txt');
u = dane_verification(:,1);
y = dane_verification(:,2);
P = length(u);

y_mod = [];
y_mod(1:4) = y(1:4);
for k = 5 : P
    y_mod(k) = w(1) * u(k-3) + w(2) * u(k-4) + w(3) * y_mod(k-1) + w(4) * y_mod(k-2);
end

figure;
stairs(y, 'b');
hold on;
stairs(y_mod, '--r');

y_mod = y_mod';
E = (y_mod - y)' * (y_mod - y);
title(strcat('E_{oe} = ', num2str(E)));


figure;
plot(y, y_mod, '.');
xlabel('Dane');
ylabel('Model OE');
title(strcat('Dane weryfikujące:  E_{oe} = ', num2str(E)));